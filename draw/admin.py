from django.contrib import admin
from draw.models import Court, Player

# Register your models here.


class CourtAdmin(admin.ModelAdmin):
    list_display = ('number',)


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'gender', 'active')


admin.site.register(Court)
admin.site.register(Player, PlayerAdmin)
