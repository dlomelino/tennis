from django import forms
from draw import models
from django.utils.safestring import mark_safe


class CustomChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return mark_safe("<img src='%s'" % obj.itemImage.url)


class PlayerSelectForm(forms.ModelForm):
    items = CustomChoiceField(widget=forms.RadioSelect, queryset=models.Player.objects.all())

    class Meta:
        model = models.Player