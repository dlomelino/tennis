# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('draw', '0004_player_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='active',
            field=models.BooleanField(),
            preserve_default=True,
        ),
    ]
