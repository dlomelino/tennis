# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('draw', '0002_player_gender'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='gender',
            field=models.CharField(max_length=6, null=True, choices=[(b'M', b'Male'), (b'F', b'Female')]),
        ),
    ]
