# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('draw', '0005_auto_20150601_2027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='active',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
