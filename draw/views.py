from django.contrib.staticfiles.templatetags.staticfiles import static
from django.template import RequestContext
from django.shortcuts import render, render_to_response
from draw import models
import random
import os.path
from tennis.settings import BASE_DIR
# Create your views here.


def home(request):
    players_all = models.Player.objects.all()
    players_men = players_all.filter(gender="M", active=1).order_by('lname', 'fname')
    players_women = players_all.filter(gender="F", active=1).order_by('lname', 'fname')
    num_players_men = players_men.count()
    num_players_women = players_women.count()

    for player in players_men:
        player = attach_image(player)
    for player in players_women:
        player = attach_image(player)

    return render(request, 'home.html', {
        'players': players_all,
        'players_men': players_men,
        'players_women': players_women,
        'num_players_men': num_players_men,
        'num_players_women': num_players_women,
    })


def play(request):
    override = request.POST.get("override", "")
    gender = request.POST.get("gender", "")
    format = request.POST.get("format", "")
    courts = request.POST.get("courts", "")
    players_men = request.POST.get("players_men", "").split(',')
    players_women = request.POST.get("players_women", "").split(',')

    if players_men:
        random.shuffle(players_men)
    if players_women:
        random.shuffle(players_women)

    if gender == 'mens':
        players = players_men
    elif gender == 'womens':
        players = players_women
    elif gender == 'mixed':
        pass

    if courts and courts not in ('', '0'):
        # Number of courts was provided
        if format == 'singles':
            players_per_court = 2
        elif format == 'doubles':
            players_per_court = 4
    else:
        # Number of courts was not provided
        if format == 'singles':
            courts = len(players) / 2
        elif format == 'doubles':
            courts = len(players) / 4

    print 'COURTS: %d' % (courts,)
    assigned_courts = []
    for i in range(int(courts)):
        assigned_courts.append([])
        if format == 'doubles':
            players_on_team = players_per_court / 2
        else:
            players_on_team = players_per_court
        for x in range(players_on_team):
            assigned_courts[i].append([])
            assigned_courts[i][x].append(players[:1])
            del players[:1]
            if format == 'doubles':
                assigned_courts[i][x].append(players[:1])
                del players[:1]

    data = {
        "assigned_courts": assigned_courts
    }

    return render_to_response('play.html', context_instance=RequestContext(request, {'data': data}))


def attach_image(player):
    filename_base = BASE_DIR
    filename = static('draw/img/players/' + player.fname[:1].lower() + player.lname.lower() + '.jpg')
    if os.path.isfile(filename_base + filename):
        player.image = filename
    else:
        player.image = static('draw/img/players/blank.jpg')

    return player