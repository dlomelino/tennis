from django.db import models

# Create your models here.


class Court(models.Model):
    number = models.IntegerField()


class Player(models.Model):
    fname = models.CharField(max_length=50, null=False)
    lname = models.CharField(max_length=50, null=False)
    gender = models.CharField(max_length=6, null=True, choices=(('M', 'Male'), ('F', 'Female')))
    active = models.BooleanField(default=True)

    def full_name(self):
        return "%s %s" % (self.fname, self.lname)

    def __unicode__(self):
        return "%s %s" % (self.fname, self.lname)