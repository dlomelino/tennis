from django.conf.urls import patterns, include, url
from django.contrib import admin
from draw.views import home, play

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', home),
    url(r'^play', play)
)
